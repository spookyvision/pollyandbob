from django.conf import settings
from django.contrib.auth import get_user_model
from django.contrib.sites.models import Site
from django.core.mail import send_mail
from django.core.urlresolvers import reverse
from django.template.loader import render_to_string
from django.utils.translation import ugettext as _

from apps.groups.models import Group
from pollyandbob.celery import app


@app.task
def invite_neighbour(user_id, neighbour_id, group_id):
    """
    Send the user an invitation to join group
    """
    user = get_user_model().objects.get(pk=user_id)
    neighbour = get_user_model().objects.get(pk=neighbour_id)
    group = Group.objects.get(pk=group_id)

    send_mail(
        subject=_('[Polly & Bob] Invitation to %s' % group.name),
        message=render_to_string('groups/emails/invitation.html', {
            'user': user,
            'neighbour': neighbour,
            'group': group,
            'invitation_link': reverse('group:invite', args=(group_id,)),
            'domain': Site.objects.get_current().domain
        }),
        from_email=settings.DEFAULT_FROM_EMAIL,
        recipient_list=[user.email],
        fail_silently=False
    )
