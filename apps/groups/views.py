from django.contrib.auth.mixins import LoginRequiredMixin
from django.http import HttpResponseRedirect
from django.views.generic import TemplateView, View
from django.views.generic.detail import DetailView, SingleObjectMixin

from .models import Group
from .forms import UsersForm
from .tasks import invite_neighbour


class GroupView(DetailView):
    model = Group

    @property
    def is_user_in_group(self):
        if not self.request.user.is_anonymous():
            return self.request.user.is_in_group(self.object)
        return False

    @property
    def is_group_admin(self):
        if not self.request.user.is_anonymous():
            return self.request.user.is_group_admin(self.object)
        return False

    @property
    def is_blocked(self):
        if not self.request.user.is_anonymous() \
                and self.request.user.is_group_blocked(self.object):
            return True
        return False

    def get_context_data(self, **kwargs):
        group = self.get_object()
        members = list(group.members.values_list('id', flat=True))
        admins = list(group.admins.values_list('id', flat=True))
        pks = members + admins
        neighbours = self.request.user.neighbours().exclude(id__in=pks)

        kwargs.update({
            'is_in_group': self.is_user_in_group,
            'is_in_group_admins': self.is_group_admin,
            'is_blocked': self.is_blocked,
            'neighbours': neighbours
        })
        return super(GroupView, self).get_context_data(**kwargs)


class BlockView(LoginRequiredMixin, View):
    """
    View to join group
    """
    def get(self, request, *args, **kwargs):
        group = Group.objects.get(pk=kwargs.get('pk'))
        request.user.blocked_groups.add(group)
        return HttpResponseRedirect(group.get_absolute_url())


class UnblockView(LoginRequiredMixin, View):
    """
    View to join group
    """
    def get(self, request, *args, **kwargs):
        group = Group.objects.get(pk=kwargs.get('pk'))
        request.user.blocked_groups.remove(group)
        return HttpResponseRedirect(group.get_absolute_url())


class JoinView(LoginRequiredMixin, View):
    """
    View to join group
    """
    def get(self, request, *args, **kwargs):
        group = Group.objects.get(pk=kwargs.get('pk'))
        group.members.add(request.user)
        return HttpResponseRedirect(group.get_absolute_url())


class LeaveView(View):
    """
    View to leave group
    """
    def get(self, request, *args, **kwargs):
        group = Group.objects.get(pk=kwargs.get('pk'))
        group.members.remove(request.user)
        group.admins.remove(request.user)
        return HttpResponseRedirect(group.get_absolute_url())


class CommentView(LoginRequiredMixin, SingleObjectMixin, View):
    """
    View to leave group
    """
    model = Group

    def get_form(self):
        return CommentForm(
            data=self.request.POST, user=self.request.user,
            group=self.get_object())

    def post(self, request, *args, **kwargs):
        form = self.get_form()
        if form.is_valid():
            form.save()
        return HttpResponseRedirect(self.get_object().get_absolute_url())


class InviteView(LoginRequiredMixin, View):
    """
    View to leave group
    """
    def post(self, request, *args, **kwargs):
        group = Group.objects.get(pk=kwargs.get('pk'))

        form = UsersForm(data=request.POST, user=request.user)
        if form.is_valid():
            neighbours = form.cleaned_data['users']
            for neighbour_id in neighbours:
                invite_neighbour.apply_async(kwargs={
                    'user_id': request.user.id,
                    'neighbour_id': neighbour_id,
                    'group_id': group.id
                })
        return HttpResponseRedirect(group.get_absolute_url())


class EditView(TemplateView):
    """
    View to leave group
    """
    template_name = 'groups/create.html'

    def get(self, request, *args, **kwargs):
        pass


class CreateView(TemplateView):
    """
    View to leave group
    """
    template_name = 'groups/create.html'

    def get_form(self):
        return GroupForm(data=request.POST or None)

    def get_context_data(self, **kwargs):
        kwargs.update({
            'form': self.get_form()
        })
        return super(CreateView, self).get_context_data(**kwargs)
