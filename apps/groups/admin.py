from django.contrib import admin

from .models import Group, GroupThread

admin.site.register([Group, GroupThread])
