# -*- coding: utf-8 -*-
from braces.views import JSONResponseMixin


class AjaxFormViewMixin(JSONResponseMixin):
    def form_valid(self, form):
        self.object = form.save()
        return self.render_json_response({
            'message': 'success!',
            'result': True
        })

    def form_invalid(self, form):
        return self.render_json_response({
            'message': 'fail',
            'errors': form.errors,
            'result': False
        })

    def get_context_data(self, **kwargs):
        return {'form': self.form_class()}
