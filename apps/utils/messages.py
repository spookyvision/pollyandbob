# -*- coding: utf-8 -*-
from django.contrib import messages as django_messages

BOOTSTRAP_LEVEL_INFO = 'alert-info'


def django_message(request, message, django_level=None, bootstrap_level=BOOTSTRAP_LEVEL_INFO):
    if not django_level:
        django_level = {
            BOOTSTRAP_LEVEL_DANGER: django_messages.ERROR
        }.get(bootstrap_level, django_messages.INFO)
    django_messages.add_message(request, django_level, message, extra_tags=bootstrap_level)


BOOTSTRAP_LEVEL_SUCCESS = 'alert-success'
BOOTSTRAP_LEVEL_WARNING = 'alert-warning'
BOOTSTRAP_LEVEL_DANGER = 'alert-danger'
