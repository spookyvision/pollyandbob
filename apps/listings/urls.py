from django.conf.urls import url

from .views import (
    ListingCreateView, SearchView, toggle_selected_special_category,
    toggle_selected_category, feed, update_filter_distance, update_categories,
    listing)


urlpatterns = [
    url(r'^listing/create$',
        ListingCreateView.as_view(),
        name='listing_create'),

    url(r'^search$', SearchView.as_view(),
        name='search'),

    url(r'^toggle_selected_category/(?P<category>[0-9]+)$',
        toggle_selected_category,
        name='toggle_selected_category'),

    url(r'^toggle_selected_special_category/(?P<category>[0-9A-Za-z_\-]+)$',
        toggle_selected_special_category,
        name='toggle_selected_special_category'),

    url(r'^feed$',
        feed, name='feed'),

    url(r'^update_filter_distance$',
        update_filter_distance,
        name='update_filter_distance'),

    url(r'^update_categories$',
        update_categories,
        name='update_categories'),

    url(r'^(?P<listing_id>[0-9]+)/$', listing, name='pb_listing'),
]
