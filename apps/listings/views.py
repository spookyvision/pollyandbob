from django.conf import settings
from django.contrib.auth import get_user_model
from django.contrib.auth.decorators import login_required
from django.contrib.auth.mixins import LoginRequiredMixin
from django.contrib.gis.db.models.functions import Distance
from django.contrib.gis.geos import Point
from django.db.models import Q, Max
from django.db.models.functions import Coalesce
from django.http import HttpResponseRedirect, JsonResponse
from django.shortcuts import render
from django.views.decorators.http import require_POST
from django.views.generic import TemplateView, View

import apps.utils.encoders
from apps.groups.models import Group
from .models import Category, Listing
from .forms import (
    FilterDistanceForm, ListingCreateForm, OptionToolCategoriesForm
)


class SearchView(LoginRequiredMixin, View):
    """
    Look for the word in:
    - user
        - name
        - about me
    - listings

    and order by date
    """
    def around_position(self):
        return self.request.user.position or Point(13.4533461, 52.5132939)

    def distance_tup(self, distance):
        return (self.around_position(), distance)

    def get_users(self, q, distance=settings.MAX_DISTANCE):
        return get_user_model().objects.filter(
                user__position__distance_lte=self.distance_tup(distance))\
            .exclude(user__id=self.request.user.id) \
            .filter(
                Q(user__first_name__icontains=q) \
                |Q(user__last_name__icontains=q) \
                |Q(user__title__icontains=q) \
                |Q(user__email__icontains=q) \
                |Q(about__icontains=q)) \
            .annotate(
                distance=Distance('user__position', self.around_position())) \
            .order_by('-user__created_at')

    def get_groups(self, q, distance=settings.MAX_DISTANCE):
        return Group.objects \
            .filter(
                position__distance_lte=self.distance_tup(distance),
            ) \
            .filter(Q(name__icontains=q)|Q(description__icontains=q)) \
            .order_by('-created_at')

    def get_listings(self, q, distance=settings.MAX_DISTANCE, category=''):
        return Listing.objects \
            .filter(
                position__distance_lte=self.distance_tup(distance),
                category__value__icontains=category
            ) \
            .filter(Q(title__icontains=q)|Q(text__icontains=q)) \
            .annotate(distance=Distance('position', self.around_position())) \
            .order_by('-created_at')

    def prepare_results(self, users, groups, listings):
        results = list(users) + list(groups) + list(listings)
        results.sort(key=lambda i: i.timestamp, reverse=True)
        return [l.to_list_display() for l in results]

    def get(self, request, *args, **kwargs):

        q = request.GET.get('q')
        category = request.GET.get('category', '')

        distance = int(request.GET.get('distance', settings.MAX_DISTANCE))
        if not 1 < distance < settings.MAX_DISTANCE:
            distance = settings.MAX_DISTANCE

        users = self.get_users(q, distance)
        groups = self.get_groups(q, distance)
        listings = self.get_listings(q, distance, category)

        more_users = self.get_users(q)
        more_groups = self.get_groups(q, settings.MAX_DISTANCE)
        more_listings = self.get_listings(q, settings.MAX_DISTANCE)

        results = self.prepare_results(users, groups, listings)
        more_results = self.prepare_results(more_users, groups, more_listings)

        data = {
            'results': results,
            'more_results': more_results
        }
        return JsonResponse(data, encoder=apps.utils.encoders.SmartJSONEncoder)


class ListingCreateView(LoginRequiredMixin, TemplateView):
    """
    Create listing
    """
    template_name = 'listings/listing_create.html'

    def get_form(self):
        return ListingCreateForm(data=self.request.POST or None,
                                 files=self.request.FILES or None,
                                 creator=self.request.user)

    def get_context_data(self, **kwargs):
        kwargs.update({
            'form': self.get_form()
        })
        return super(ListingCreateView, self).get_context_data(**kwargs)

    def post(self, request, *args, **kwargs):
        form = self.get_form()
        if form.is_valid():
            return self.form_valid(form)
        return self.form_invalid(form)

    def form_valid(self, form):
        listing = form.save()
        return HttpResponseRedirect(listing.get_absolute_url())

    def form_invalid(self, form):
        context = self.get_context_data()
        context.update({
            'form': form
        })
        return self.render_to_response(context)


def add_listing(request):
    request.is_ajax()


@login_required
@require_POST
def update_categories(request):
    form = OptionToolCategoriesForm(request.POST, instance=request.user)
    if form.is_valid():
        form.save()
        request.session['categories'] = []
        request.session['special_category_groups'] = False
        request.session['special_category_neighbors'] = False
        return render(request, 'listings/selected_categories.html')
    else:
        raise Exception


def listing(request, listing_id):
    return render(request, 'listings/listing.html', {'listing':Listing.objects.get(pk=listing_id)})


@login_required
@require_POST
def update_filter_distance(request):
    user = request.user
    form = FilterDistanceForm(request.POST)
    if form.is_valid():
        user.filter_distance = float(form.cleaned_data['distance'])*1000
        user.save()
        return render(request, 'listings/feed_content.html')
    else:
        raise Exception


@login_required
@require_POST
def toggle_selected_category(request, category):
    user_categories = set(request.session.get('categories', []))
    user_categories_new = user_categories ^ set(category)
    request.session['categories'] = list(user_categories_new)
    return render(request, 'listings/selected_categories.html')


@login_required
@require_POST
def toggle_selected_special_category(request, category):
    allowed_keys = {'special_category_neighbors', 'special_category_groups'}
    if category in allowed_keys:
        request.session[category] = not request.session.get(category, False)
        return render(request, 'listings/selected_categories.html')
    else:
        raise Exception


@login_required
def feed(request):
    user = request.user
    around_position = request.user.position
    distance_tup = (around_position, user.filter_distance)
    user_categories = request.session.get('categories', [])
    show_neighbors = request.session.get('special_category_neighbors', False)
    show_groups = request.session.get('special_category_groups', False)
    nothing_selected = False
    if user_categories:
        user_categories = Category.objects.filter(pk__in=user_categories)
    else:
        nothing_selected = not show_neighbors and not show_groups
        if nothing_selected:
            user_categories =  user.categories.all()

    user_categories_view = [CategoryContext.from_category(
            c, user, session=request.session) for c in user.categories.all()]
    blocked_groups = user.blocked_groups.values_list('pk', flat=True)
    listings = Listing.objects\
        .filter(position__distance_lte=distance_tup) \
        .filter(category__in = user_categories) \
        .annotate(latest_comment_date=Max('comments__created_at')) \
        .annotate(timestamp=Coalesce('latest_comment_date','created_at')) \
        .annotate(distance=Distance('position', around_position)) \
        .order_by('-timestamp')

    groups = []
    if user.special_category_groups:
        user_categories_view.insert(0, CategoryContext.groups(user, session=request.session))
        if nothing_selected or show_groups:
            groups = Group.objects \
                .filter(position__distance_lte=distance_tup) \
                .exclude(pk__in=blocked_groups) \
                .annotate(timestamp=Coalesce('latest_comment_date','created_at')) \
                .annotate(distance=Distance('position', around_position)) \
                .order_by('-timestamp')
    # .annotate(latest_comment_date=Max('threads__comments__created_at')) \

    neighbors = []
    if user.special_category_neighbors:
        user_categories_view.insert(0, CategoryContext.neighbors(user, session=request.session))
        if nothing_selected or show_neighbors:
            neighbors = get_user_model().objects\
                .filter(user__position__distance_lte=distance_tup) \
                .exclude(id=user.id) \
                .annotate(distance=Distance('user__position', around_position)) \
                .order_by('user__created_at')

    all_items = list(listings) + list(neighbors) + list(groups)
    # TODO in my opinion this approach has huge disadvantage in scale
    # as list will be stored in memory and sorting will be must slower
    # than plain database query sort order
    all_items.sort(key=lambda i: i.timestamp, reverse=True)
    all_items = [l.to_list_display() for l in all_items]
    return render(request, 'listings/listings.html', {
        'items': all_items,
        'categories': user_categories_view,
        'all_categories': CategoryContext.all(user),
        'filter_distance_km': user.filter_distance / 1000.
    })


class CategoryContext:
    def __init__(self, value, count, db_ref, user, session=None, is_special=True):
        self.value = value
        self.count = count
        self.db_ref = db_ref
        self.is_special = is_special
        self.user = user
        self.session = session
        self.set_checked()

    @classmethod
    def from_category(cls, cat, user, session=None):
        # TODO count should reflect the user's distance filter (unify with feed() function body)i
        return cls(value=cat.value, count=cat.listings.count(), db_ref=cat.pk,
                   user=user, session=session, is_special=False)

    @classmethod
    def from_special(cls, value, count, db_ref, user, session=None):
        return cls(value=value, count=count, db_ref=db_ref, user=user,
                   session=session, is_special=True)

    @classmethod
    def neighbors(cls, user, session=None):
        # TODO count should reflect the user's distance filter (unify with feed() function body)
        return cls.from_special(
                'Neighbors', count=get_user_model().objects.all().count(),
                db_ref='special_category_neighbors', user=user, session=session)

    @classmethod
    def groups(cls, user, session=None):
        # TODO count should reflect the user's distance filter (unify with feed() function body)
        return cls.from_special(
                'Groups', count=Group.objects.all().count(),
                db_ref='special_category_groups', user=user, session=session)

    @classmethod
    def all(cls, user):
        return [cls.neighbors(user), cls.groups(user)] + [
            cls.from_category(c, user) for c in Category.objects.all()]

    def set_checked(self,):
        is_checked = False
        if self.is_special:
            if self.session:
                is_checked = self.session.get(self.db_ref, False)
            else:
                if getattr(self.user, self.db_ref):
                    is_checked = True
        else:
            if self.session:
                is_checked = str(self.db_ref) in self.session.get('categories', [])
            else:
                is_checked = self.user.categories.filter(pk=self.db_ref).exists()
        self.checked = 'checked' if is_checked else ''
