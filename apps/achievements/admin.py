from django.contrib import admin

from .models import Achievement, AchievementAward

admin.site.register([Achievement, AchievementAward])
