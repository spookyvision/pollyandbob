from django.conf.urls import url

from apps.comet.views import open, token_valid

urlpatterns = [
    url(r'^open/$', open, name='api.open'),
    url(r'^token_valid/$', token_valid, name='token_valid'),
]
