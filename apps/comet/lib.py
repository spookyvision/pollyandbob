# ASK

import requests
from dateutil.relativedelta import relativedelta
from django.conf import settings
from django.conf.urls import url
from django.utils import timezone
from rest_framework.response import Response

from apps.comet.api_messages import *
from apps.comet.models import Token


def make_simple_urlfunc(app_name):
    def f(name):
        names = (app_name, name)
        return url(r'^%s/$' % name, '%s.views.%s' % names, name = "%s.%s" % names)
    return f

def api_response_ok(result=None):
    return Response(ok_dict(result))

def api_response_err(
        message=DEFAULT_ERROR_MESSAGE,
        code=DEFAULT_ERROR_CODE):
    result = Response(data=err_dict(message, code))
    return result

def expire_tokens():
    if Token.is_immortal(): return
    tokens = Token.objects.filter(created_at__lt=timezone.now() - relativedelta(seconds=Token.expire_secs()))
    requests.post(settings.CB_WS_URL+'expire', {'ids':tokens.ids()})
    tokens.delete()
