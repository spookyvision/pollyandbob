import datetime

from django.conf import settings
from django.contrib.auth.base_user import AbstractBaseUser
from django.contrib.gis.db import models
from django.contrib.gis.db.models.functions import Distance
from django.contrib.gis.geos import Point
from django.core.urlresolvers import reverse
from django.db.models.signals import post_save
from django.dispatch import receiver
from django.utils.translation import ugettext_lazy as _
from django_countries.fields import CountryField
from phonenumber_field.modelfields import PhoneNumberField
from sorl.thumbnail import get_thumbnail

from apps.users.managers import UserManager
from apps.listings.models import Category
from apps.utils.models import ThumbnailMixin


class User(AbstractBaseUser, ThumbnailMixin):
    USERNAME_FIELD = 'email'
    EMAIL_FIELD = 'email'

    email = models.EmailField(_("Email"), max_length=255, unique=True)
    title = models.CharField(
        _("Name"), max_length=17, help_text=_("Up to 17 characters"))
    created_at = models.DateTimeField(_("Created at"), auto_now_add=True)
    photo = models.ImageField(_("Photo"), upload_to='images', null=True)
    content = models.TextField(_("About"), null=True, blank=True)
    what_i_like = models.TextField(_("Ehat I like"), null=True, blank=True)
    birthday = models.DateField(_("Birth date"), blank=True, null=True)
    phone_number = PhoneNumberField(_("Phone number"), blank=True, null=True)
    language = models.CharField(
        _("Interface language"), max_length=3, choices=settings.LANGUAGES,
        default='en')
    languages = models.TextField(
        _('User languages'), max_length=100, null=True, blank=True)
    #  boolean values
    is_active = models.BooleanField(_("Is active"), default=True)
    is_admin = models.BooleanField(_("Is admin"), default=False)
    is_verified_phone = models.BooleanField(
        _("Is phone verified"), default=False)
    is_verified_email = models.BooleanField(
        _("Is email verified"), default=False)
    special_category_neighbors = models.BooleanField(default=True)
    special_category_groups = models.BooleanField(default=True)
    #  geo-position fields
    street = models.CharField(_("Street"), max_length=128, null=True, blank=True)
    #  TODO: validate
    zip_code = models.CharField(
        _("Postal code"), max_length=5, db_index=True, null=True, blank=True)
    city = models.CharField(_("City"), max_length=128, null=True, blank=True)
    country = CountryField(_("Country"), max_length=128, null=True, blank=True)
    position = models.PointField(_("Geo location"), null=True)
    # push_token = models.CharField(max_length=4096, null=True, blank=True)  #lol, google
    #  notification settings
    notify_me_never = models.BooleanField(
        _("Don't sent me emails"), default=False)
    notify_on_changes_in_connection_center = models.BooleanField(
        _("After each change in connection center"), default=True)
    notify_on_invite_to_listing = models.BooleanField(
        _("Only when I am invited to a listing"), default=True)
    sent_me_newsletter = models.BooleanField(
        _("Sent me regular newsletter"), default=True)
    filter_distance = models.IntegerField(_("radius"), default=1000)
    #  many to many fields
    # unread = models.ManyToManyField(
    #     Listing, blank=True, verbose_name=_("Unread listings"))
    # blocked_groups = models.ManyToManyField(
    #     Group, verbose_name=_('Blocked groups'), blank=True)
    categories = models.ManyToManyField(
        Category, verbose_name=_("Categories"), blank=True)

    objects = UserManager()

    def __str__(self):
        return self.title

    def get_absolute_url(self):
        return reverse('user_detail', args=[self.pk])

    def is_superuser(self):
        return self.is_staff

    def neighbours(self, distance=settings.MAX_DISTANCE):
        """
        Return the list of neighbours
        """
        around_position = self.user.position or Point(13.4533461, 52.5132939)
        distance_tup = (around_position, distance)

        neighbours = self.__class__.objects\
            .filter(user__position__distance_lte=distance_tup) \
            .exclude(user__id=self.user.id) \
            .annotate(distance=Distance('user__position', around_position)) \
            .order_by('-user__created_at')

        return neighbours

    def is_group_blocked(self, group):
        return self.blocked_groups.filter(id=group.id).exists()

    def is_group_admin(self, group):
        return self.user.administrated_groups.filter(id=group.id).exists()

    def is_in_group(self, group):
        if any([
            self.administrated_groups.filter(id=group.id).count(),
            self.groups.filter(id=group.id).count(),
        ]):
            return True
        return False

    @property
    def age(self):
        if self.birthday:
            bd = self.birthday, td = datetime.date.today()
            return td.year - bd.year - ((td.month, td.day) < (bd.month, bd.day))
        return None

    def community_points(self):
        """
        someday we will have a lot of users and this gonna be rewritten in the
        best way with caching or storing to field and smart updating of it, but
        now we are on rush and have to do fastest and the most reliable way
        :return: Int
        """
        return sum((
            self.listings.count() * 5,
            self.administrated_groups.count() * 5,
            self.groups.count() * 2,
            0  # invites count * 2
        ))

    @property
    def unread_notifications(self):
        return self.user.notifications\
            .filter(user=self.user, is_watched=False)

    @property
    def timestamp(self):
        return self.user.created_at

    @property
    def image(self):
        if not self.photo:
            return None
        return self.photo

    @property
    def thumbnail_small(self):
        if not self.photo:
            return None
        return get_thumbnail(self.photo, '100x100', crop='top').url

    @property
    def thumbnail_tiny(self):
        if not self.photo:
            return None
        return get_thumbnail(self.photo, '50x50', crop='top').url

    def to_list_display(self):
        category = {'url': '', 'value': 'Neighbors'}
        return {'category': category,
                'image': self.thumbnail_image,
                'title': self.user.title,
                'text': self.about,
                'distance': self.distance.m, # added by .annotate() in view. a little obscure, maybe create a view class instead
                }

    @property
    def is_verified(self):
        return self.is_verified_phone or self.is_verified_email

    def is_blocked_by(self, owner):
        return BlockedUserRelation.objects.filter(
                owner=owner, user=self).exists()

    def get_full_name(self):
        return self.email

    def get_short_name(self):
        return self.email

    def has_perm(self, perm, obj=None):
        """Does the user have a specific permission?"""
        # Simplest possible answer: Yes, always
        return True

    def has_module_perms(self, app_label):
        """Does the user have permissions to view the app `app_label`?"""
        # Simplest possible answer: Yes, always
        return True

    @property
    def is_staff(self):
        """Is the user a member of staff?"""
        # Simplest possible answer: All admins are staff
        return self.is_admin


class BlockedUserRelation(models.Model):
    """
    This model store information about blocked users
    'owner' is the one who blocked a user,
    'user' is a blocked user
    """
    owner = models.ForeignKey(
        User, verbose_name=_("Owner"), related_name='blocked_users')
    user = models.ForeignKey(
        User, verbose_name=_("User"), related_name='blocked_by',
        help_text=_("Blocked user"))

    class Meta:
        verbose_name_plural = _("Blocked users")
        verbose_name = _("Blocked user")
        unique_together = (('owner', 'user'),)

    def __str__(self):
        return " blocked by ".join(map(str([self.user, self.owner])))


@receiver(post_save, sender=User)
def activate_user_automatically(sender, instance, **kwargs):
    """
    This is tempo func to activate users automatically without email confirmation
    I'll remove it after seting up emailing system on the server
    """
    if kwargs['created']:
        instance.is_active = True
        instance.save(update_fields=['is_active'])
