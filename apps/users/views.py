from braces.views import JSONResponseMixin
from django.conf import settings
from django.contrib import auth
from django.contrib.auth import get_user_model
from django.contrib.auth.mixins import LoginRequiredMixin
from django.core.mail import send_mass_mail
from django.core.urlresolvers import reverse
from django.db import IntegrityError
from django.forms import formset_factory
from django.template.loader import render_to_string
from django.utils.translation import ugettext as _
from django.views.generic import (CreateView, DeleteView, DetailView,
                                  TemplateView, UpdateView, View)

from apps.utils.views import AjaxFormViewMixin
from .forms import (AddressForm, EmailForm, EmailNotificationsForm,
                    LanguageForm, PostalCodeForm, ChangePasswordForm,
                    ChangeEmailForm)
from .models import BlockedUserRelation


class UserDetailView(DetailView):
    model = get_user_model()

    def get_context_data(self, **kwargs):
        if self.request.user.is_authenticated():
            kwargs['is_blocked'] = self.object.is_blocked_by(self.request.user)
            kwargs['AddressForm'] = AddressForm(instance=self.request.user)
        return super().get_context_data(**kwargs)

    def get_template_names(self):
        if self.object.is_authenticated() and self.object == self.request.user:
            return ['users/own_profile.html']
        return super().get_template_names()


class UserUpdateView(LoginRequiredMixin, UpdateView):
    model = get_user_model()
    fields = ['title']

    def get_object(self, queryset=None):
        return self.request.user


class BlockUserCreateView(AjaxFormViewMixin, LoginRequiredMixin, CreateView):
    model = BlockedUserRelation
    http_method_names = ['post']
    fields = ['user']

    def form_valid(self, form):
        self.object = form.save(commit=False)
        self.object.owner = self.request.user
        try:
            self.object.save()
        except IntegrityError:
            pass
        return self.render_json_response({
            'message': 'success!',
            'result': True
        })


class ZipCodeCheckView(AjaxFormViewMixin, View):
    def get(self, request, *args, **kwargs):
        form = PostalCodeForm(request.GET)
        if form.is_valid():
            return self.form_valid(form)
        return self.form_invalid(form)

    def form_valid(self, form):
        zip_code = form.cleaned_data['zip_code']
        country = form.cleaned_data['country']
        if zip_code and country:
            user_model = get_user_model()
            if user_model.objects.filter(
                    zip_code=zip_code, country=country).count() > 100:
                return self.render_json_response({'more_then_100': True})
            return self.render_json_response({'more_then_100': False})


class BlockUserDeleteView(JSONResponseMixin, LoginRequiredMixin, DeleteView):
    slug_field = 'user'
    slug_url_kwarg = 'user'
    model = BlockedUserRelation

    def get_queryset(self):
        return super(BlockUserDeleteView, self).get_queryset().filter(
                owner=self.request.user)

    def delete(self, request, *args, **kwargs):
        try:
            self.object = self.get_object()
        except self.model.DoesNotExist:
            pass
        finally:
            self.object.delete()
        return self.render_json_response({
            'message': 'success!',
            'result': True
        })


class InviteFriends(View):
    """
    Invite friends
    """
    def prepare_message(self, email):
        return (
            _('Invitation to Polly Bob from %s' % email),
            render_to_string('email_invite', {'email': email}),
            settings.EMAIL_FROM,
            [email]
        )

    def get_formset(self):
        formset = formset_factory(EmailForm)
        return formset(data=self.request.POST or None)

    def post(self, request, *args, **kwargs):
        formset = self.get_formset()
        if formset.is_valid():
            emails = [self.prepare_message(form.cleaned_data['email']) \
                for form in formset.forms]
            # TODO must be run with celery
            # send_mass_mail(mails)


class UserDeleteView(LoginRequiredMixin, DeleteView):
    def get_object(self, queryset=None):
        return self.request.user

    def get_success_url(self):
        return reverse('landing')


class BaseUserUpdateView(AjaxFormViewMixin, LoginRequiredMixin, UpdateView):
    model = get_user_model()
    http_method_names = ['post']

    def get_object(self, queryset=None):
        return self.request.user


class UserLanguageUpdateView(BaseUserUpdateView):
    fields = ['language']


class UserWhatILikeUpdateView(BaseUserUpdateView):
    fields = ['what_i_like']


class UserAboutUpdateView(BaseUserUpdateView):
    fields = ['content']


class BaseUserUpdateView(AjaxFormViewMixin, LoginRequiredMixin, UpdateView):
    model = get_user_model()

    def get_object(self, queryset=None):
        return self.request.user


class PasswordChangeView(BaseUserUpdateView):
    form_class = ChangePasswordForm
    http_method_names = ['post']  # we save form from settings view

    def form_valid(self, form):
        result = super().form_valid(form)
        user = self.object
        user = auth.authenticate(email=user.email,
                                 password=form.cleaned_data['new_password'])
        auth.login(self.request, user)
        return result


class AddressChangeView(BaseUserUpdateView):
    form_class = AddressForm
    http_method_names = ['post']  # we save form from settings view
#
#
class UserSettingsView(LoginRequiredMixin, TemplateView):
    template_name = 'users/settings.html'

    def get_context_data(self, **kwargs):
        kwargs['ChangePasswordForm'] = ChangePasswordForm()
        kwargs['EmailNotificationsForm'] = EmailNotificationsForm(
                instance=self.request.user)
        kwargs['ChangeEmailForm'] = ChangeEmailForm()
        kwargs['LanguageForm'] = LanguageForm(instance=self.request.user)
        return super(UserSettingsView, self).get_context_data(**kwargs)


class EmailUpdateView(BaseUserUpdateView):
    form_class = ChangeEmailForm
    http_method_names = ['post']  # we save form from settings view
    #TODO: send email confirmation


class NotificationUpdateView(BaseUserUpdateView):
    """
    we save form from settings view
    """
    form_class = EmailNotificationsForm


# @login_required
# def block(request, partner_id):
#     blocked = User.objects.get(pk=partner_id)
#     request.user.blocked.add(blocked)
#     apps.utils.messages.django_message(
#         request,
#         mark_safe(_('<strong>%s</strong> kann Dich nun nicht mehr kontaktieren. Im Einstellungsmenü kannst Du Deine Sperren bearbeiten.') % conditional_escape(blocked.display_name)))
#     return HttpResponseRedirect('/')
#
#
# @login_required
# def edit_blocked(request, extra_context=None):
#     if request.method == 'POST':
#         unblocked = User.objects.get(pk=request.POST.get('unblock'))
#         request.user.blocked.remove(unblocked)
#         apps.utils.messages.django_message(
#             request,
#             mark_safe(_('Ok, <strong>%s</strong> kann Dich wieder kontaktieren.') % conditional_escape(unblocked.display_name)))
#         return HttpResponseRedirect('/')
#     else:
#         if extra_context is None:
#             extra_context = {}
#         context = {'blocked': request.user.blocked.all()}
#         context.update(extra_context)
#         return render(request, 'edit_blocked.html', context)
