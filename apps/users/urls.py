from django.conf.urls import url

from .views import (
    ZipCodeCheckView, UserUpdateView, UserWhatILikeUpdateView,
    UserAboutUpdateView, UserLanguageUpdateView, UserDeleteView,
    PasswordChangeView, EmailUpdateView, BlockUserCreateView, UserSettingsView,
    BlockUserDeleteView, InviteFriends, UserDetailView, NotificationUpdateView)

urlpatterns = [

    url(r'^check-zip$',
        ZipCodeCheckView.as_view(), name='check_zip'),

    url(r'^update-user$',
        UserUpdateView.as_view(), name='user_update'),

    url(r'^update-user-what-i-like$',
        UserWhatILikeUpdateView.as_view(), name='user_what_i_like_update'),

    url(r'^update-user-about$',
        UserAboutUpdateView.as_view(), name='user_about_update'),

    url(r'^update-user-language$',
        UserLanguageUpdateView.as_view(), name='user_language_update'),

    url(r'^delete-user',
        UserDeleteView.as_view(), name='user_delete'),

    url(r'^settings$',
        UserSettingsView.as_view(), name='user_settings'),

    url(r'^settings/change-password$',
        PasswordChangeView.as_view(), name='user_change_password'),

    url(r'^settings/change-email$',
        EmailUpdateView.as_view(), name='user_change_email'),

    url(r'^user-block-create$',
        BlockUserCreateView.as_view(), name='user_block_create'),

    url(r'^user-block-delete/(?P<user>\d+)$',
        BlockUserDeleteView.as_view(), name='user_block_delete'),

    url('^invite/$',
        InviteFriends.as_view(), name='invite_friends'),

    url(r'^settings/notifications$',
        NotificationUpdateView.as_view(), name='user_notifications'),

    url(r'^id(?P<pk>\d+)',
        UserDetailView.as_view(), name='user_detail'),
]
