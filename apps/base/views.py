from django.forms import formset_factory
from django.views.generic import TemplateView
from django.http import HttpResponseRedirect
from django.core.urlresolvers import reverse
from django.utils.translation import ugettext as _

from apps.users.forms import EmailForm
from apps.users.tasks import send_email


class LandingView(TemplateView):
    template_name = 'base/landing.html'

    def get_invite_friends_formset(self):
        formset = formset_factory(EmailForm, extra=3)
        return formset(data=self.request.POST or None, prefix='invite_friends')

    def get_context_data(self, **kwargs):
        kwargs.update({
            'invite_friends_formset': self.get_invite_friends_formset()
        })
        return super(LandingView, self).get_context_data(**kwargs)

    def invite_friend(self, form):
        if 'email' in form.cleaned_data:
            send_email.apply_async(kwargs={
                'subject': _('Invitation to Polly & Bob'),
                'message_template': 'users/emails/invite_to_site.html',
                'to': form.cleaned_data['email']
            })

    def post(self, request, *args, **kwargs):
        formset = self.get_invite_friends_formset()
        if formset.is_valid():
            [self.invite_friend(form) for form in formset]
        return HttpResponseRedirect(reverse('landing'))
