"use strict";
/**
 * We have to separate this file partially to common.js and to specific apps static
 */

$(function() {
    // I don't remember what's that
    $('.slide').removeClass('active');
    // range slider it seems
    $('input[type="range"]').rangeslider({
        polyfill: false,
        rangeClass: 'rangeslider',
        fillClass: 'rangeslider__fill',
        handleClass: 'rangeslider__handle',
        onInit: function() {

        },
        onSlide: function(position, value) {
            $('.rangeslider__handle').attr('data-content', value).html("<span class='km_cls'>km</span>");
            $('.done').removeClass('active');
            $('.slide').addClass('active');
        },
        onSlideEnd: function(position, value) {
            $('.done').addClass('active');
            $('.slide').removeClass('active');
        }
    });

    //accordination
    var menu_ul = $('.hide_part'),
        menu_a = $('.open21');

    menu_ul.hide();

    menu_a.click(function(e) {
        e.preventDefault();
        if (!$(this).hasClass('active21')) {
            menu_a.removeClass('active21');
            menu_ul.filter(':visible').slideUp('normal');
            $(this).addClass('active21').next().stop(true, true).slideDown('normal');
        } else {
            $(this).removeClass('active21');
            $(this).next().stop(true, true).slideUp('normal');
        }
    });

    //select bootstrap
    $("#checkAll").click(function() {
        $('.drp input:checkbox').not(this).prop('checked', this.checked);
    });

    //create listing Invite Some Neighbors
    $('.dropdown.mega-dropdown4 a').on('click', function(event) {
        $(this).parent().toggleClass("open5");
    });
    $('body').on('click', function(e) {
        if (!$('.dropdown.mega-dropdown4').is(e.target) && $('.dropdown.mega-dropdown4').has(e.target).length === 0 && $('.open5').has(e.target).length === 0) {
            $('.dropdown.mega-dropdown4').removeClass('open5');
        }
    });

    //paypal js
    $('.change1').click(function() {
        $('body').toggleClass('show_radio');
        return false;
    });
    $('.change2').click(function() {
        $('body').toggleClass('show_radio2');
        return false;
    });
    $(".save_btn1").click(function() {
        var radioValue = $("input[name='gender']:checked").val();
        if (radioValue) {
            $('.radio_value').toggleClass('show_radio2');
            alert("Your are a - " + radioValue);
        }
    });

    //scrollbar
    $('#scrollbar1, #scrollbar2').tinyscrollbar();

});

//range slider
// jQuery(function() {
//     jQuery("#slider_single2").flatslider({
//         min: 0.1,
//         max: 20,
//         value: 0.1,
//         step: 0.1,
//         range: "&Kappa;&Mu;",
//         einheit: '&Kappa;&Mu;'
//     });
// });



//toogle hide show comment block
// $(document).ready(function() {
//     $('.comment_hide').hide();
//     $('.rht a').click(function() {
//         $(this).addClass('openbtn');
//         var target = "#" + $(this).data("target");
//         $(".comment_hide").not(target).hide();
//         $(target).show();
//     });
//
//     $('.closebtn').click(function() {
//         $('.rht a').removeClass('openbtn');
//         var target = "#" + $(this).data("target");
//         $(".comment_hide").not(target).hide();
//         $(target).hide();
//     });
//
//     $('.closebtn').click(function() {
//
//     });
//
// });


//toogle hide show comment block end
// $(document).ready(function() {
//     $('.dropdown.mega-dropdown a').on('click', function(event) {
//         $(this).parent().toggleClass("open4");
//     });
//
//     $('body').on('click', function(e) {
//         if (!$('.dropdown.mega-dropdown').is(e.target) && $('.dropdown.mega-dropdown').has(e.target).length === 0 && $('.open4').has(e.target).length === 0) {
//             $('.dropdown.mega-dropdown').removeClass('open4');
//         }
//     });
// });

// $(document).ready(function() {
//     $('.dropdown.mega-dropdown2 a').on('click', function(event) {
//         $(this).parent().toggleClass("open");
//     });
//
//     $('body').on('click', function(e) {
//         if (!$('.dropdown.mega-dropdown2').is(e.target) && $('.dropdown.mega-dropdown2').has(e.target).length === 0 && $('.open').has(e.target).length === 0) {
//             $('.dropdown.mega-dropdown2').removeClass('open');
//         }
//     });
// });
//
// $(document).ready(function() {
//     $('.dropdown.mega-dropdown3 a').on('click', function(event) {
//         $(this).parent().toggleClass("open3");
//     });
//
//     $('body').on('click', function(e) {
//         if (!$('.dropdown.mega-dropdown3').is(e.target) && $('.dropdown.mega-dropdown3').has(e.target).length === 0 && $('.open3').has(e.target).length === 0) {
//             $('.dropdown.mega-dropdown3').removeClass('open3');
//         }
//     });
// });


//
// //More comment
// $(document).ready(function() {
//     var vis = 5;
//     $('.comment_sect article').slice(vis).hide();
//
//     var $loadmore = $('<a href="javascript:;" class="view_more_comment">show more comments</a>')
//     $loadmore.click(function() {
//         $('.comment_sect article:hidden').slice(0, vis).show();
//         if ($('.comment_sect article:hidden').length == 0)
//             $loadmore.hide();
//     });
//     $('.comment_sect article:last-child').after($loadmore);
// });
//
// //second comment
// $(document).ready(function() {
//     var vis2 = 1;
//     $('.comment_sect2 section').slice(vis2).hide();
//     var $loadmore2 = $('<a href="javascript:;" class="view_more_comment more_ans">More Answers</a>');
//     $loadmore2.click(function() {
//         $('.comment_sect2 section:hidden').slice(0, vis2).show();
//         if ($('.comment_sect2 section:hidden').length == 0)
//             $loadmore2.hide();
//     });
//     $('.comment_sect2 section:last-child').after($loadmore2);
//
//     $('.more_ans2').click(function() {
//         $('.answer_script_con').show();
//         if ($('.answer_script_con').show()) {
//             $('.comment_sect2 section').show();
//             $('.view_more_comment, .review_hide').hide();
//         }
//     });
//
//     $('.close_answer').click(function() {
//         $('.comment_sect2').hide();
//         $('.review_com, .review_hide').show();
//     });
//
// });
//
// $(document).ready(function() {
//     $('.answer_script').click(function() {
//         $('.answer_script_con').show();
//         $('.review_hide').hide();
//     });

// });
