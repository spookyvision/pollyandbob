from os.path import dirname, abspath, join

BASE_DIR = dirname(dirname(dirname(abspath(__file__))))

STATIC_URL = '/static/'

STATICFILES_DIRS = (
    join(BASE_DIR, 'components/bower_components'),
)

STATICFILES_FINDERS = (
    'django.contrib.staticfiles.finders.FileSystemFinder',
    'django.contrib.staticfiles.finders.AppDirectoriesFinder',
    'djangobower.finders.BowerFinder',
    'compressor.finders.CompressorFinder',
)
STATIC_ROOT = join(BASE_DIR, 'static')

BOWER_COMPONENTS_ROOT = join(BASE_DIR, 'components')


# PIPELINE = {
#     # 'PIPELINE_ENABLED': True,
#     'COMPILERS': (
#         'pipeline.compilers.less.LessCompiler',
#     ),
#     'STYLESHEETS': {
#         'app': {
#             'source_filenames': (
#                 'css/app.less',
#             ),
#             'output_filename': 'css/app.css',
#         },
#     },
#     'JAVASCRIPT': {
#         'app': {
#             'source_filenames': (
#                 'js/jquery-2.1.4.min.js',
#                 'js/js.cookie.js',
#                 'js/community.js',
#                 'js/rangeslider.js',
#                 'js/customize.js',
#                 'js/bootstrap-multiselect.js',
#                 'js/jquery.tinyscrollbar.min.js',
#                 'js/jquery-ui.min.js',
#                 'js/common.js',
#                 'js/jquery.tinyscrollbar.min.js',
#
#                 'twitter_bootstrap/js/transition.js',
#                 'twitter_bootstrap/js/modal.js',
#                 'twitter_bootstrap/js/dropdown.js',
#                 'twitter_bootstrap/js/scrollspy.js',
#                 'twitter_bootstrap/js/tab.js',
#                 'twitter_bootstrap/js/tooltip.js',
#                 'twitter_bootstrap/js/popover.js',
#                 'twitter_bootstrap/js/alert.js',
#                 'twitter_bootstrap/js/button.js',
#                 'twitter_bootstrap/js/collapse.js',
#                 'twitter_bootstrap/js/carousel.js',
#                 'twitter_bootstrap/js/affix.js',
#             ),
#             'output_filename': 'js/app.js',
#         },
#     }
# }

MEDIA_URL = '/media/'
MEDIA_ROOT = join(BASE_DIR, 'media')
