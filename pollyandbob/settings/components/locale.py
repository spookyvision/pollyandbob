from django.utils.translation import ugettext_lazy as _

LANGUAGE_CODE = 'en-us'
LANGUAGES = [
    ('de', _('German')),
    ('en', _('English')),
]

USE_I18N = True

USE_L10N = True

LOCALE_PATHS = ('apps/base/locale', )

PHONENUMBER_DEFAULT_REGION = 'DE'
