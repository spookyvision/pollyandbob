from split_settings.tools import optional, include

# we have to set INSTALLED_APPS here or else PyCharm can't recognize tags
INSTALLED_APPS = (
    'django.contrib.admin',
    'django.contrib.auth',
    'django.contrib.contenttypes',
    'django.contrib.sessions',
    'django.contrib.humanize',
    'django.contrib.sites',
    'django.contrib.messages',
    'django.contrib.staticfiles',
    'django.contrib.gis',

    'sorl.thumbnail',
    'django_extensions',
    'django_cleanup',
    'push_notifications',
    'django_countries',
    'djangobower',
    'compressor',
    'mathfilters',
    'rosetta',
    'countries_plus',

    'apps.base',
    'apps.achievements',
    'apps.comet',
    'apps.users',
    'apps.groups',
    'apps.listings',
    'apps.notifications',
    'allauth',
    'allauth.account',
    'bootstrap3',
    'leaflet',
    'fluent_comments',
    'crispy_forms',
    'django_comments',
    'threadedcomments',
)

include(
    'components/*.py',

    # local settings (do not commit to version control)
    optional('local_settings.py'),

    scope=globals()
)
