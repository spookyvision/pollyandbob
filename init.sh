#!/bin/bash
set -e
read -p "prerequisites: postgres, postgis, virtualenv (python3), fabric, redis, npm. All good? " -n 1 -r
echo ''
if [[ $REPLY =~ ^[Yy]$ ]]
then
    mkvirtualenv pollyandbob
    pip install -r requirements.txt
    npm install -g gulp bower
    npm install
    mkdir static
    fab prepare_assets
    createdb pollyandbob
    psql -U postgres -d pollyandbob -c 'create extension postgis; create extension postgis_topology;'
    ./manage.py migrate
fi
